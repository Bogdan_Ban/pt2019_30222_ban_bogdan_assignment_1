package ro.tuc.pt._assign1;

public class Term {
	private float coef;
	private int degree;
	
	public Term(float c, int d) throws gradPozitiv {
		coef = c;
		if(d < 0)
			throw new gradPozitiv("Gradul trebuie sa fie pozitiv!");
		else
			degree = d;
	}
	
	public float getCoef() {
		return coef;
	}
	
	public int getDegree() {
		return degree;
	}
	
	public void setCoef(float c) {
		coef = c;
	}
	
	public void setDegree(int d) {
		degree = d;
	}
	
}

