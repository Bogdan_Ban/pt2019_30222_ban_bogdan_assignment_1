package ro.tuc.pt._assign1;

import java.util.ArrayList;
import java.util.regex.*;

public class Model {
	
	public static ArrayList<Term> regex(String input) throws gradPozitiv {
		ArrayList<Term> arl = new ArrayList<Term>();
		Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
		Matcher m = p.matcher( input );

		while (m.find()) {
		    int coef = Integer.parseInt(m.group(1));
		    int degree = Integer.parseInt(m.group(2));
		    arl.add(new Term(coef,degree));
		}
		
		return arl;
	}
	
}
