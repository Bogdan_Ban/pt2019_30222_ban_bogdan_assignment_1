package ro.tuc.pt._assign1;

import java.util.ArrayList;
import java.util.Collections;

import junit.framework.TestCase;

public class Testare extends TestCase {
	
	Polinom p1, p2;
	
	public void setUp() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(-3, 0));
		t.add(new Term(-2, 1));
		t.add(new Term(4, 2));
		t.add(new Term(3, 3));
		
		ArrayList<Term> r = new ArrayList<Term>();
		r.add(new Term(-2, 0));
		r.add(new Term(1, 1));
		r.add(new Term(-3, 2));
		
		p1 = new Polinom(t);
		p2 = new Polinom(r);
	}
	
	public void testAddition() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(-5, 0));
		t.add(new Term(-1, 1));
		t.add(new Term(1, 2));
		t.add(new Term(3, 3));
		Polinom p3 = new Polinom(t);
		Polinom sum = Operations.add(p1, p2);
		assertTrue(p3.getTerms().get(0).getCoef() == sum.getTerms().get(0).getCoef());
		assertTrue(p3.getTerms().get(1).getCoef() == sum.getTerms().get(1).getCoef());
		assertTrue(p3.getTerms().get(2).getCoef() == sum.getTerms().get(2).getCoef());
		assertTrue(p3.getTerms().get(3).getCoef() == sum.getTerms().get(3).getCoef());
		
	}
	
	public void testSubtraction() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(-1, 0));
		t.add(new Term(-3, 1));
		t.add(new Term(7, 2));
		t.add(new Term(3, 3));
		Polinom p3 = new Polinom(t);
		Collections.sort(p3.getTerms(), new SortByDegree());
		Polinom subt = Operations.sub(p1, p2);
		assertTrue(p3.getTerms().get(0).getCoef() == subt.getTerms().get(0).getCoef());
		assertTrue(p3.getTerms().get(1).getCoef() == subt.getTerms().get(1).getCoef());
		assertTrue(p3.getTerms().get(2).getCoef() == subt.getTerms().get(2).getCoef());
		assertTrue(p3.getTerms().get(3).getCoef() == subt.getTerms().get(3).getCoef());
	}
	
	public void testMultiplication() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(6, 0));
		t.add(new Term(1, 1));
		t.add(new Term(-1, 2));
		t.add(new Term(4, 3));
		t.add(new Term(-9, 4));
		t.add(new Term(-9, 5));
		Polinom p3 = new Polinom(t);
		Polinom mul = Operations.multilpy(p1, p2);
		assertTrue(p3.getTerms().get(0).getCoef() == mul.getTerms().get(0).getCoef());
		assertTrue(p3.getTerms().get(1).getCoef() == mul.getTerms().get(1).getCoef());
		assertTrue(p3.getTerms().get(2).getCoef() == mul.getTerms().get(2).getCoef());
		assertTrue(p3.getTerms().get(3).getCoef() == mul.getTerms().get(3).getCoef());
		
	}
	
	public void testDerivation() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(-2, 0));
		t.add(new Term(8, 1));
		t.add(new Term(9, 2));
		Polinom p3 = new Polinom(t);
		Polinom der = Operations.derivatives(p1);
		assertTrue(p3.getTerms().get(0).getCoef() == der.getTerms().get(0).getCoef());
		assertTrue(p3.getTerms().get(1).getCoef() == der.getTerms().get(1).getCoef());
		assertTrue(p3.getTerms().get(2).getCoef() == der.getTerms().get(2).getCoef());
	}
	
	public void testIntegration() throws gradPozitiv {
		ArrayList<Term> t = new ArrayList<Term>();
		t.add(new Term(-3, 1));
		t.add(new Term(-1, 2));
		t.add(new Term((float)4/3, 3));
		t.add(new Term((float)3/4, 4));
		Polinom p3 = new Polinom(t);
		Polinom integr = Operations.integral(p1);
		assertTrue(p3.getTerms().get(0).getCoef() == integr.getTerms().get(0).getCoef());
		assertTrue(p3.getTerms().get(1).getCoef() == integr.getTerms().get(1).getCoef());
		assertTrue(p3.getTerms().get(2).getCoef() == integr.getTerms().get(2).getCoef());
		assertTrue(p3.getTerms().get(3).getCoef() == integr.getTerms().get(3).getCoef());
	}
	
}
