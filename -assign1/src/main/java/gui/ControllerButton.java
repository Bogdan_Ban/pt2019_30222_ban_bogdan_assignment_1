package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import ro.tuc.pt._assign1.*;

public class ControllerButton{
	private View view;
	private Model model;
	
	public ControllerButton(View v, Model m){
		view = v;
		model = m;
		view.addAdditionListener(new AdditionListener());
		view.addSubtractionListener(new SubtractionListener());
		view.addMultiplyListener(new MultiplyListener());
		view.addDerivativesListener(new DerivativesListener());
		view.addIntegralListener(new IntegralListener());
	}
	
	public class AdditionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		
			String s1 = view.getText1().getText();
			String s2 = view.getText2().getText();
			
			ArrayList<Term> arr1 = new ArrayList<Term>();
			try {
				arr1 = Model.regex(s1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ArrayList<Term> arr2 = new ArrayList<Term>();
			try {
				arr2 = Model.regex(s2);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Polinom p1 = new Polinom(arr1);p1.print();System.out.println();
			Polinom p2 = new Polinom(arr2);p2.print();System.out.println();
			Operations.add(p1, p2);
			p1.print();
			view.getLabel().setText(p1.toString());
			Operations.sub(p1, p2);

		}
	}
	
	public class SubtractionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		
			String s1 = view.getText1().getText();
			String s2 = view.getText2().getText();
			
			ArrayList<Term> arr1 = new ArrayList<Term>();
			try {
				arr1 = Model.regex(s1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ArrayList<Term> arr2 = new ArrayList<Term>();
			try {
				arr2 = Model.regex(s2);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Polinom p1 = new Polinom(arr1);p1.print();System.out.println();
			Polinom p2 = new Polinom(arr2);p2.print();System.out.println();
			Operations.sub(p1, p2);
			p1.print();
			view.getLabel().setText(p1.toString());
			Operations.add(p1, p2);

		}
	}
	
	public class MultiplyListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		
			String s1 = view.getText1().getText();
			String s2 = view.getText2().getText();
			
			ArrayList<Term> arr1 = new ArrayList<Term>();
			try {
				arr1 = Model.regex(s1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ArrayList<Term> arr2 = new ArrayList<Term>();
			try {
				arr2 = Model.regex(s2);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Polinom p1 = new Polinom(arr1);p1.print();System.out.println();
			Polinom p2 = new Polinom(arr2);p2.print();System.out.println();
			Polinom p3 = new Polinom(new ArrayList<Term>());
			try {
				p3 = Operations.multilpy(p1, p2);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			p3.print();
			view.getLabel().setText(p3.toString());
		}
	}
	
	public class DerivativesListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		
			String s1 = view.getText1().getText();
			
			ArrayList<Term> arr1 = new ArrayList<Term>();
			try {
				arr1 = Model.regex(s1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Polinom p1 = new Polinom(arr1);p1.print();System.out.println();
			Polinom der = new Polinom(new ArrayList<Term>());
			try {
				der = Operations.derivatives(p1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			der.print();
			view.getLabel().setText(der.toString());

		}
	}
	
	public class IntegralListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		
			String s1 = view.getText1().getText();
			
			ArrayList<Term> arr1 = new ArrayList<Term>();
			try {
				arr1 = Model.regex(s1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Polinom p1 = new Polinom(arr1);p1.print();System.out.println();
			Polinom integr = new Polinom(new ArrayList<Term>());
			try {
				integr = Operations.integral(p1);
			} catch (gradPozitiv e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			integr.print();
			view.getLabel().setText(integr.toString());
		}
	}
	
}
