package ro.tuc.pt._assign1;

import java.util.Comparator;

public class SortByDegree implements Comparator<Term>{

	public int compare(Term t1, Term t2) {
		// TODO Auto-generated method stub
		return t2.getDegree() - t1.getDegree();
	}

}
