package gui;
import ro.tuc.pt._assign1.*;

public class Appl {

	public static void main(String[] args) {
		
		Model model = new Model();
		View view = new View("Polynomials");
		ControllerButton controller = new ControllerButton(view, model);
		view.setVisible(true);
	}

}
