package ro.tuc.pt._assign1;

import java.util.ArrayList;
import java.util.Iterator;

public class Operations {
	
	public static Polinom add(Polinom p1, Polinom p2) {
		Polinom res = p1.copy();
		Iterator<Term> it = p2.getTerms().iterator();
		while(it.hasNext()) {
			res.addTerm(it.next());
		}
		return res;
	}
	
	public static Polinom sub(Polinom p1, Polinom p2) {
		Polinom res = p1.copy();
		Iterator<Term> it = p2.getTerms().iterator();
		while(it.hasNext()) {
			res.subTerm(it.next());
		}
		return res;
	}
	
	public static Polinom multilpy(Polinom p1, Polinom p2) throws gradPozitiv {
			
		Polinom mul = new Polinom(new ArrayList<Term>());
		for(Term a : p1.getTerms()) {
			for(Term b : p2.getTerms()) {
				Term t = new Term(0,0);
				t.setCoef(a.getCoef() * b.getCoef());
				t.setDegree(a.getDegree() + b.getDegree());
				mul.addTerm(t);
			}
		}
		return mul;
	}
	
	public static Polinom derivatives(Polinom p1) throws gradPozitiv {
		
		Polinom der = new Polinom(new ArrayList<Term>());
		for(Term a : p1.getTerms()) {
			if(a.getDegree() > 0) {
				Term t = new Term(0,0);
				t.setDegree(a.getDegree() - 1);
				t.setCoef(a.getCoef() * a.getDegree());
				der.addTerm(t);
			}
		}
		return der;
	}
	
	public static Polinom integral(Polinom p1) throws gradPozitiv {
			
			Polinom der = new Polinom(new ArrayList<Term>());
			for(Term a : p1.getTerms()) {
				Term t = new Term(0,0);
				t.setDegree(a.getDegree() + 1);
				t.setCoef(a.getCoef() / (a.getDegree() + 1));
				der.addTerm(t);
			}
			return der;
		}
}

