package ro.tuc.pt._assign1;

import java.util.ArrayList;
import java.util.Collections;

public class Polinom {
	private ArrayList <Term> terms;
	
	public Polinom(ArrayList<Term> arr) {
		terms = new ArrayList<Term>();
		terms = arr;
	}
	
	public ArrayList<Term> getTerms(){
		return terms;
	}
	
	public void addTerm(Term t) {
		boolean ok = false;
		for(Term a : terms) {
			if(a.getDegree() == t.getDegree()) {
				a.setCoef(a.getCoef() + t.getCoef());
				ok = true;
			}
		}
		if(ok == false) {
			terms.add(t);
		}
	}
	
	public void subTerm(Term t) {
		boolean ok = false;
		for(Term a : terms) {
			if(a.getDegree() == t.getDegree()) {
				a.setCoef(a.getCoef() - t.getCoef());
				ok = true;
			}
		}
		if(ok == false) {
			t.setCoef((-1)*t.getCoef());
			terms.add(t.getDegree(),t);
		}
		Collections.sort(terms, new SortByDegree());
	}
	
	public Polinom copy() {
		Polinom p = new Polinom(terms);
		return p;
	}
	
	public void print() {
		for(Term a : terms) {
			if(a.getCoef() != 0) {
				System.out.print(" + " + a.getCoef() + " * x^" + a.getDegree());
			}else {
				System.out.print(" " + a.getCoef() + " * x^" + a.getDegree());
			}
		}
	}
	
	public String toString() {
		String s = "";
		for(Term a : terms) {
			if(a.getCoef() != 0) {
				s +=" + " + a.getCoef() + " * x^" + a.getDegree();
			}else {
				s += " " + a.getCoef() + " * x^" + a.getDegree();
			}
		}
		return s;
	}
	
}
