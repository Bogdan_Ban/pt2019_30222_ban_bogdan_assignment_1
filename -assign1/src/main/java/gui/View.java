package gui;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;


public class View extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton button1 = new JButton("+");
	private JButton button2 = new JButton("-");
	private JButton button3 = new JButton("*");
	private JButton button4 = new JButton("d/dx");
	private JButton button5 = new JButton("∫");
	private JTextField pol1 = new JTextField(20);
	private JTextField pol2 = new JTextField(20);
	private JLabel label = new JLabel("result");
	private JLabel poly1 = new JLabel("Polynomial 1");
	private JLabel poly2 = new JLabel("Polynomial 2");
	
	
	
	public View(String name) {
		super(name);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 0;
		pane.add(poly1, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 0;
		c.gridy = 1;
		pane.add(poly2, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 0;
		pane.add(pol1, c);
		
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 1;
		c.gridy = 1;
		pane.add(pol2, c);
		
		c.ipady = 30;
		c.ipadx = 40;
		c.gridx = 1;
		c.gridy = 2;
		pane.add(label, c);
		
		c.fill = (GridBagConstraints.LAST_LINE_START);
		c.ipady = 20;
		c.ipadx = 20;
		c.gridx = 3;
		c.gridy = 0;
		pane.add(button1, c);
		
		
		c.fill = (GridBagConstraints.FIRST_LINE_END);
		c.gridx = 4;
		c.gridy = 0;
		pane.add(button2, c);
		
		c.fill = (GridBagConstraints.LAST_LINE_START);
		c.gridx = 3;
		c.gridy = 1;
		pane.add(button3, c);
		
		c.fill = (GridBagConstraints.LAST_LINE_END);
		c.gridx = 4;
		c.gridy = 1;
		pane.add(button4, c);
		
		c.fill = (GridBagConstraints.LAST_LINE_END);
		c.gridx = 3;
		c.gridy = 2;
		pane.add(button5, c);
		
		this.add(pane);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();

	}
	
	public JTextField getText1(){
		return pol1;
	}
	public JTextField getText2(){
		return pol2;
	}
	public JLabel getLabel(){
		return label;
	}
	
	void addAdditionListener(ActionListener l) {
		button1.addActionListener(l);
	}
	void addSubtractionListener(ActionListener l) {
		button2.addActionListener(l);
	}
	void addMultiplyListener(ActionListener l) {
		button3.addActionListener(l);
	}
	void addDerivativesListener(ActionListener l) {
		button4.addActionListener(l);
	}
	void addIntegralListener(ActionListener l) {
		button5.addActionListener(l);
	}
	
}
